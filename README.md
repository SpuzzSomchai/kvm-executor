# KVM Executor

Based off of https://docs.gitlab.com/runner/executors/custom_examples/libvirt.html

I made some modifications to make it work on my system.

The major modification was to allow a job to specify a BASE_VM variable to tell the runner which VM image to use as the base.

I changed the naming of the VMs to add the project name instead of ID and the job slug.

I enabled graphics on the cloned VM for debugging purposes.

I also changed the startup ssh check to not actually ssh in, but just use netcat to make sure it was repsonding on port 22.

I changed the timeout waiting for IP address to 60s so if I tried to run multiple jobs in parallel and they all started at the same time, it would not time out.

I also changed the shutdown behavior, to ssh in and issue a shutdown command to make sure the VM shutdown. Otherwise it was still running and the commands to delete the disk image were failing.
